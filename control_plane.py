# Name:
# PennKey:

import argparse
import json
import os
import sys
import threading
from time import sleep

sys.path.append("utils")
import bmv2
import helper
from convert import *


def RunControlPlane(router, id_num, p4info_helper):
    # TODO

    """
    Example of how to set an LPM entry:
    table_entry = p4info_helper.buildTableEntry(
            table_name = "cis553Ingress.tiIpv4Lpm",
            match_fields = { "hdr.ipv4.dstAddr": [ ip, prefix_len ] },
            action_name = ...,
            action_params = ...)
    """

    while 1:
        # just stay running forever
        sleep(1)

    router.shutdown()

# Starts a control plane for each switch. Hardcoded for our Mininet topology.
def ConfigureNetwork(p4info_file = "build/data_plane.p4info",
                     bmv2_json = "build/data_plane.json"):
    p4info_helper = helper.P4InfoHelper(p4info_file)

    threads = []

    print "Connecting to P4Runtime server on r1..."
    r1 = bmv2.Bmv2SwitchConnection('r1', "127.0.0.1:50051", 0)
    r1.MasterArbitrationUpdate()
    r1.SetForwardingPipelineConfig(p4info = p4info_helper.p4info,
                                   bmv2_json_file_path = bmv2_json)
    t = threading.Thread(target=RunControlPlane, args=(r1, 1, p4info_helper))
    t.start()
    threads.append(t)

    print "Connecting to P4Runtime server on r2..."
    r2 = bmv2.Bmv2SwitchConnection('r2', "127.0.0.1:50052", 1)
    r2.MasterArbitrationUpdate()
    r2.SetForwardingPipelineConfig(p4info = p4info_helper.p4info,
                                   bmv2_json_file_path = bmv2_json)
    t = threading.Thread(target=RunControlPlane, args=(r2, 2, p4info_helper))
    t.start()
    threads.append(t)

    print "Connecting to P4Runtime server on r3..."
    r3 = bmv2.Bmv2SwitchConnection('r3', "127.0.0.1:50053", 2)
    r3.MasterArbitrationUpdate()
    r3.SetForwardingPipelineConfig(p4info = p4info_helper.p4info,
                                   bmv2_json_file_path = bmv2_json)
    t = threading.Thread(target=RunControlPlane, args=(r3, 3, p4info_helper))
    t.start()
    threads.append(t)

    for t in threads:
        t.join()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CIS553 P4Runtime Controller')

    parser.add_argument("-b", '--bmv2-json',
                        help="path to BMv2 switch description (json)",
                        type=str, action="store",
                        default="build/data_plane.json")
    parser.add_argument("-c", '--p4info-file',
                        help="path to P4Runtime protobuf description (text)",
                        type=str, action="store",
                        default="build/data_plane.p4info")

    args = parser.parse_args()

    if not os.path.exists(args.p4info_file):
        parser.error("File %s does not exist!" % args.p4info_file)
    if not os.path.exists(args.bmv2_json):
        parser.error("File %s does not exist!" % args.bmv2_json)
    
    ConfigureNetwork(args.p4info_file, args.bmv2_json)
